@extends('layout.master')
@section('title')
Update Profile
@endsection

@push('script')

<script src="https://cdn.tiny.cloud/1/343h2xqhw02pnckze30z5mzi3928zsxauqry48s3o75ojoqx/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>

<script>
  tinymce.init({
    selector: 'textarea',
    plugins: 'a11ychecker advcode casechange export formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
    toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter pageembed permanentpen table',
    toolbar_mode: 'floating',
    tinycomments_mode: 'embedded',
    tinycomments_author: 'Author name',
  });
</script>
@endpush

@section('content')
<form method="POST" action="/profile/"{{$profile->id}}>
    @csrf
    @method('put')
    <div class="form-group">
        <label >Nama User </label>
        <input type="text" class="form-control" value="{{$profile->user->name}}" >
      </div>
      <div class="form-group">
        <label >Email User</label>
        <input type="ntext" class="form-control" value="{{$profile->user->email}}" >
      </div>


    <div class="form-group">
      <label >"Umur" </label>
      <input type="number"
      name="umur" class="form-control" value="{{$profile->umur}}">
    </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label >"Biodata" </label>
        <textarea
        name="bio" class="form-control">{{$profile->bio}}
      </div>
      @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Alamat</label>
        <textarea name="alamat" class="form-control">{{$profile->alamat}}</textarea>
      </div>    
      @error('alamat')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror


    <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection