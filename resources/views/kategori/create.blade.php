@extends('layout.master')
@section('title')
Halaman Tambah Kategori
@endsection

@push('style')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />    
@endpush

@push('script')
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script>
  // In your Javascript (external .js resource or <script> tag)
$(document).ready(function() {
    $('.js-example-basic-single').select2();
});
</script>
@endpush

@section('content')

<form method="Post" action="/kategori">
    @csrf
    <div class="form-group">
      <label>Nama Kategori</label>
      <input type="text" name="nama" class="js-example-basic-single" style="width: 100%">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Deskripsi</label>
      <textarea name="deskripsi" cols="30" rows="10" class="form-control"></textarea>
    </div>
    @error('deskripsi')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>



@endsection