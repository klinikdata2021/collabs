@extends('layout.master')
@section('title')
Halaman List Kategori
@endsection
@section('content')

<a href="/kategori/create" class="btn btn-primary #b-3">Tambah Kategori</a>

<table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Deskripsi</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
      @forelse ($kategori as $key => $item)
        <tr>
            <td>{{$key + 1}}</td>
            <td>{{$item->nama}}</td>    
            <td>{{$item->deskripsi}}</td>
            <td>
                
            
                <form class="mt-2" action="/kategori/{{$item->id}}" method="POST">
                <a href="/kategori/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                <a href="/kategori/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                    @csrf
                    @method('delete')
                <input type="submit" value="delete" class="btn btn-danger btn-sm">
                </form>
                
            </td>

        </tr>
      @empty
          <h1>Data Tidak Ada</h1>
      @endforelse
    </tbody>
  </table>



@endsection
