@extends('layout.master')
@section('title')
Halaman Edit Kategori
@endsection
@section('content')

<form method="Post" action="/kategori/{{$kategori->id}}">
    @csrf
    @method('put')
    <div class="form-group">
      <label>Nama Kategori</label>
      <input type="text" name="nama" value="{{$kategori->nama}}" class="form-control">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Deskripsi</label>
      <textarea name="deskripsi" cols="30" rows="10" class="form-control">{{$kategori->deskripsi}}</textarea>
    </div>
    @error('deskripsi')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>



@endsection