@extends('layout.master')
@section('title')
Halaman Edit Data Pemain film Baru
@endsection
@section('content')
<form method="POST" action="/berita/{{$berita->id}}">
    @csrf
    @method('put')
    <div class="form-group">
      <label >"Nama" </label>
      <input type="text" value="{{$berita->nama}}"
      name="nama" class="form-control">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label >"umur" </label>
        <input type="text" value="{{$berita->umur}}"
        name="umur" class="form-control">
      </div>
      @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>bio</label>
        <textarea name="bio" cols="30" rows="10" class="form-control">{{$berita->nama}}</textarea>
      </div>    
      @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror


    <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection