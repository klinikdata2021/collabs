@extends('layout.master')
@section('title')
Halaman Detail Show
@endsection
@section('content')


<h1>{{$berita->nama}}</h1>
<p>{{$berita->umur}}</p>
<p>{{$berita->bio}}</p>

@auth
<form method='POST' action="komentar">
@csrf
<input type="hidden" value="{{$berita->id}}" name="berita_id">
<div class="form-group">
    <textarea name='isi' cols='40' rows='10' class='form-control' placeholder="isi">
    </div>
@endauth

@endsection