<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cast extends Model
{
    protected $table = "komentar";
    protected $fillabel = ["berita_id", "user_id", "isi"];


    public function kategori()
   {
       return $this->belongsTo('App\Kategori', 'kategori_id');
   }


}
