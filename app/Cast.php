<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cast extends Model
{
    protected $table = 'berita';
    protected $fillable = ['judul', 'content', 'thumbnail', 'kategori_id'];

    public function kategori()
   {
       return $this->belongsTo('App\Kategori', 'kategori_id');
   }


}
