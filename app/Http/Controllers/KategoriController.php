<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kategori;
use RealRashid\SweetAlert\Facades\Alert;


class KategoriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kategori = Kategori::all();

        return view('kategori.index',compact('kategori'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('kategori.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'deskripsi' => 'required|max:255',
          
        ],
        [
            'nama.required' => 'Nama Harus diisi',
            'deskripsi.required'  => 'deskripsi tidak boleh kosong',
            'deskripsi.max'  => 'karakter tidak boleh lebih dari 255',  
        ]
    );
    $kategori = new Kategori;
 
        $kategori->nama = $request->nama;
        $kategori->deskripsi = $request->deskripsi;

        $kategori->save();

    return redirect('/kategori');
    Alert::success('Tambah', 'Tambah Data Kategori Jalan');


}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $kategori = Kategori::find($id)->first();
        return view('kategori.show', compact('kategori'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kategori = Kategori::find($id);
        return view('kategori.edit', compact('kategori'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
            'deskripsi' => 'required|max:255',
          
        ],
        [
            'nama.required' => 'Nama Harus diisi',
            'deskripsi.required'  => 'deskripsi tidak boleh kosong',
            'deskripsi.max'  => 'karakter tidak boleh lebih dari 255',  
        ]
        );
        $kategori =Kategori::find($id);
        $kategori->nama = $request['nama'];
        $kategori->deskripsi = $request['deskripsi'];
        $kategori->save();

        return redirect('/kategori');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kategori = Kategori::find($id);
 
        $kategori->delete();

        return redirect('/kategori');    
    }
}
