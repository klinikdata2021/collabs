<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cast;

class Castcontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $berita = Berita::all();
        
        return view('Berita.index',compact('Berita'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function create()
    {
        return view('cast.create');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    
        $validatedData = $request->validate([
            'nama' => 'required|max:45',
            'umur' => 'required',
            'bio' => 'required',    
        ],
        [
            'nama.required' => 'nama harus diisi dan tidak boleh dari 45 karakter',
            'umur.required'  => 'umur harus diisi',
            'bio.required' => 'bio harus diisi',
        ]
       );

    $berita = new Berita;
 
    $berita->nama = $request->nama;
    $berita->umur = $request->umur;
    $berita->bio = $request->bio;

    $berita->save();


    return redirect('/cast');
    


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $berita = Cast::find($id)->first();

        return view('berita.show', compact ('berita'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $berita = Berita::find($id);

        return view('berita.edit', compact ('berita'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'nama' => 'required|max:45',
            'umur' => 'required',
            'bio' => 'required',    
        ],
        [
            'nama.required' => 'nama harus diisi dan tidak boleh dari 45 karakter',
            'umur.required'  => 'umur harus diisi',
            'bio.required' => 'bio harus diisi',
        ]
      );

      $berita = Berita::find($id);
 
      $berita->nama = $request['nama'];
      $berita->umur = $request['umur'];
      $berita->bio = $request['bio'];
    
      $berita->save();

      return redirect ('/cast');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $berita = Berita::find($id);
 
        $berita->delete();

        return redirect('/berita');
    }
}
