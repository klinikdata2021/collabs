<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'DashboardController@index');
Route::get('/biodata', 'FormController@bio');
Route::post('/kirim', 'FormController@kirim');

Route::get('/data-table', 'DashboardController@table');

Route::group(['middleware' => ['auth']], function () {
//CRUD Kategori

Route::get('/kategori/create', 'KategoriController@create'); //mengarah ke form tambah data
Route::post('/kategori', 'KategoriController@store'); //menyimpan data form ke database table kategori

//READ

Route::get('/kategori', 'KategoriController@index'); //ambil data ke database ditampilkan ke blasde
Route::get('/kategori/{kategori_id}', 'KategoriController@show'); //route detail 

//update
Route::get('/kategori/{kategori_id}/edit', 'KategoriController@edit'); //route detail 
Route::put('/kategori/{kategori_id}', 'KategoriController@update');

//Delete
Route::delete('/kategori/{kategori_id}', 'KategoriController@destroy'); //route detail

//profile
Route::resource('/profile', 'ProfileController')->only (['index', 'update']);

//komentar
Route::resource('/komentar', 'KomentarController')->only (['store']);
});

//CRUD Berita
Route::resource('berita', 'BeritaController');

Auth::routes();

